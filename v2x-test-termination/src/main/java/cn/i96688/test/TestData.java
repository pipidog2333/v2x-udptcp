package cn.i96688.test;

import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.IOException;

import org.apache.commons.lang3.RandomUtils;

import com.google.gson.Gson;

import cn.i96688.v2x.utils.MsgUtil;
import cn.i96688.v2x.v2xframe.AccelerationSet4Way;
import cn.i96688.v2x.v2xframe.BSM;
import cn.i96688.v2x.v2xframe.BrakeSystemStatus;
import cn.i96688.v2x.v2xframe.ElevationConfidence;
import cn.i96688.v2x.v2xframe.HeartBeat;
import cn.i96688.v2x.v2xframe.Login;
import cn.i96688.v2x.v2xframe.MessageFrame;
import cn.i96688.v2x.v2xframe.Position3D;
import cn.i96688.v2x.v2xframe.PositionConfidence;
import cn.i96688.v2x.v2xframe.PositionConfidenceSet;
import cn.i96688.v2x.v2xframe.Response;
import cn.i96688.v2x.v2xframe.TransmissionState;
import cn.i96688.v2x.v2xframe.VehicleClassification;
import cn.i96688.v2x.v2xframe.VehicleSize;


public class TestData {
	public static final String TOKEN = "8E1A5A50";
	public static String ID="1101101102450001";
	public static String PASSWORD="abc123";
	public static void main(String[] args) throws Exception {
	}

	public static byte[] getLogin() {
		Login login=new Login();
		login.id=MsgUtil.str2hexBinary(ID);
		login.password=PASSWORD.getBytes();
		ByteArrayOutputStream bout = new ByteArrayOutputStream();	
		try {
			MessageFrame.login(login).per_encode(false, bout);
		} catch (IOException e) {
			e.printStackTrace();
		};
		return bout.toByteArray();
	}
	public static byte[] getHeartBeat() {
		HeartBeat hb=new HeartBeat();
		hb.id=MsgUtil.str2hexBinary(ID);
		ByteArrayOutputStream bout = new ByteArrayOutputStream();	
		try {
			MessageFrame.heartBeat(hb).per_encode(false, bout);
		} catch (IOException e) {
			e.printStackTrace();
		};
		return bout.toByteArray();
	}
	public static byte[] getBSM() {
		BSM bsm = new BSM();
		
		
		bsm.id=MsgUtil.str2hexBinary(ID);
		bsm.msgCnt=12;
		bsm.secMark=10;
		Position3D pos=new Position3D();
		pos.elevation=1;
		pos.lat=10001;
		pos.long_=15550;
		bsm.pos=pos;
		bsm.plateNo="AB0011".getBytes();
		PositionConfidenceSet pcs=new PositionConfidenceSet();
		pcs.pos=PositionConfidence.a200m;
		pcs.elevation=ElevationConfidence.elev_000_01;
		bsm.accuracy=pcs;
		bsm.transmission=TransmissionState.park;
		bsm.speed=2;
		bsm.heading=1;
		AccelerationSet4Way accelSet=new AccelerationSet4Way();
		accelSet.lat=401;
		accelSet.long_=1990;
		accelSet.yaw=1;
		accelSet.vert=4;
		bsm.accelSet=accelSet;
		bsm.brakes=new BrakeSystemStatus();
		VehicleSize size=new VehicleSize();
		size.height=5;
		size.length=5;
		size.width=5;
		bsm.size=size;
		VehicleClassification vclass=new VehicleClassification();
		vclass.classification=5;
		bsm.vehicleClass=vclass;
		
		
		/*------ BSM message encode sample .------*/
		MessageFrame bsmFrame = MessageFrame.bsmFrame(bsm);
		ByteArrayOutputStream bout = new ByteArrayOutputStream();	
		byte[] bsmout = null;
		try {
			bsmFrame.encode(bout);
			bsmout=bout.toByteArray();
			
			MessageFrame mf=MessageFrame.decode(new ByteArrayInputStream(bsmout));
			System.out.println(new Gson().toJson(mf));
			
		} catch (IOException e) {
			e.printStackTrace();
		}
		
		return bsmout;
	}
}
