package cn.i96688.v2x.module.msg.service.impl;

import java.io.ByteArrayInputStream;

import org.apache.dubbo.config.annotation.Service;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.google.gson.Gson;

import cn.i96688.v2x.api.DataService;
import cn.i96688.v2x.module.msg.service.MessageService;
import cn.i96688.v2x.v2xframe.MessageFrame;

@Service
public class MessageServiceImpl implements DataService,MessageService {
	private final static Logger LOGGER = LoggerFactory.getLogger(MessageServiceImpl.class);
	@Override
	public void save(byte[] msg) {
		LOGGER.info("模拟业务处理开始......");
		try {
			MessageFrame mf=MessageFrame.decode(new ByteArrayInputStream(msg));
			LOGGER.info("处理从前端传入的数据：{}",new Gson().toJson(mf));
		}catch(Exception e) {
			LOGGER.error("数据处理出错",e);
		}
		LOGGER.info("模拟业务处理完成");
	}

}
